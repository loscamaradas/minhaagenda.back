﻿using FluentValidation;
using MinhaAgenda.Models.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhaAgenda.Models.Validation
{
    public class CreatePersonValidator: AbstractValidator<CreatePersonModel>
    {

        public CreatePersonValidator()
        {
            RuleFor(person => person.Name).NotNull().WithErrorCode("VAL01")
                                          .NotEmpty().WithErrorCode("VAL01");

            RuleFor(person => person.Phone).NotNull().WithErrorCode("VAL02")
                                           .Length(10, 11).WithErrorCode("VAL02");

        }

    }
}
