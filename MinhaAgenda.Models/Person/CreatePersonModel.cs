﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhaAgenda.Models.Person
{
    public class CreatePersonModel
    {

        public string Name { get; set; }

        public string Phone { get; set; }

    }
}
