﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhaAgenda.Models.Report
{
    public class Error
    {

        public string Code { get; set; }

        public string Message { get; set; }

        public string Description { get; set; }

        public IList<ErrorDetails> Details { get; set; }

    }
}
