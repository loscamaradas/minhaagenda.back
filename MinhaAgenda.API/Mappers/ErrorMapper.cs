﻿using FluentValidation.Results;
using MinhaAgenda.Models.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MinhaAgenda.API.Mappers
{
    public static class ErrorMapper
    {

        public static Error MapValidationResultsToError(ValidationResult result)
        {

            var error = new Error()
            {
                Message = "Validation Errors",
                Code = "100",
                Details = new List<ErrorDetails>()
            };

            foreach(var validationError in result.Errors)
            {
                var errorDetails = new ErrorDetails()
                {
                    Message = validationError.ErrorMessage,
                    Code = validationError.ErrorCode,
                    Field = validationError.PropertyName
                };

                error.Details.Add(errorDetails);

            }

            return error;

        }

    }
}