﻿using MinhaAgenda.Models.Person;
using MinhaAgenda.Services.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MinhaAgenda.API.Mappers
{
    public static class PersonMapper
    {

        public static Person MapCreatePersonModelToEntity(CreatePersonModel model)
        {
            if (model == null) return null;

            var person = new Person()
            {
                Name = model.Name,
                Phone = model.Phone
            };

            return person;
        }

        public static GetPersonModel MapEntityToGetPersonModel(Person person)
        {
            if (person == null) return null;

            var model = new GetPersonModel()
            {
                Name = person.Name,
                Phone = person.Phone
            };

            return model;

        }

    }
}