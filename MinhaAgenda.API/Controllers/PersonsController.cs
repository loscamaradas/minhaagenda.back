﻿using FluentValidation;
using MinhaAgenda.API.Mappers;
using MinhaAgenda.Models.Person;
using MinhaAgenda.Services.Entities;
using MinhaAgenda.Services.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace MinhaAgenda.API.Controllers
{

    [RoutePrefix("Persons")]
    public class PersonsController : ApiController
    {

        private readonly IPersonService _personService;

        private readonly IValidator<CreatePersonModel> _personValidator;

        public PersonsController(IPersonService personService,
                                 IValidator<CreatePersonModel> personValidator)
        {
            _personService = personService;
            _personValidator = personValidator;
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult GetAllPersons()
        {

            try
            {

                var models = new List<GetPersonModel>();

                var persons = _personService.GetAllPersons();

                foreach (var person in persons)
                {

                    var model = PersonMapper.MapEntityToGetPersonModel(person);

                    models.Add(model);

                }

                return Ok(models);

            }
            catch (Exception ex)
            {
                return InternalServerError();
            }

        }

        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult GetPersonById(int id)
        {

            try
            {

                var person = _personService.GetPersonById(id);

                var model = PersonMapper.MapEntityToGetPersonModel(person);

                return Ok(model);
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }

        }

        [HttpPost]
        [Route("")]
        public IHttpActionResult CreatePerson(CreatePersonModel model)
        {
            try
            {

                var validationResults = _personValidator.Validate(model);

                if (validationResults.Errors.Count > 0)
                {
                    var errors = ErrorMapper.MapValidationResultsToError(validationResults);

                    return Content((HttpStatusCode)422, errors);
                }

                var person = PersonMapper.MapCreatePersonModelToEntity(model);

                var insertedPerson = _personService.CreatePerson(person);

                var insertedModel = PersonMapper.MapEntityToGetPersonModel(insertedPerson);

                return Ok(insertedModel);
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }

    }
}