﻿using MinhaAgenda.Services.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MinhaAgenda.Services.Entities;
using System.Data;
using Dapper;
using System.Data.SqlClient;

namespace MinhaAgenda.Data.Repositories
{
    public class PersonRepository : IPersonRepository
    {

        private readonly IDbConnection _dbConnection;

        public PersonRepository()
        {
            string connectionString = @"Server=localhost\SQLEXPRESS;Database=AgendaDb;Trusted_Connection=True;";

            _dbConnection = new SqlConnection(connectionString);

        }

        public IEnumerable<Person> GetAllPersons()
        {
            string query = "SELECT * FROM Person";

            _dbConnection.Open();

            var persons = _dbConnection.Query<Person>(query);

            _dbConnection.Close();

            return persons;

        }

        public Person GetPersonById(int id)
        {
            string query = @"SELECT * FROM Person
                            WHERE Id = @Id";

            _dbConnection.Open();

            var persons = _dbConnection.Query<Person>(query, new { Id = id });

            _dbConnection.Close();

            return persons.FirstOrDefault();
        }

        public void InsertPerson(Person person)
        {

            string query = @"INSERT INTO Person
                            (
                                Name,
                                Phone
                            ) 
                            VALUES
                             (
                                @Name,
                                @Phone
                             )";

            _dbConnection.Open();

            var id = _dbConnection.Query<int>(query, new
            {
                Name = person.Name,
                Phone = person.Phone
            });

            _dbConnection.Close();

        }
    }
}
