﻿using MinhaAgenda.Services.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MinhaAgenda.Services.Entities;
using MinhaAgenda.Services.Interfaces.Repositories;

namespace MinhaAgenda.Services
{
    public class PersonService : IPersonService
    {

        private readonly IPersonRepository _personRepository;

        public PersonService(IPersonRepository personRepository)
        {
            _personRepository = personRepository;
        }

        public Person CreatePerson(Person person)
        {
            _personRepository.InsertPerson(person);

            return person;
        }

        public IEnumerable<Person> GetAllPersons()
        {

            var persons = _personRepository.GetAllPersons();

            return persons;
        }

        public Person GetPersonById(int id)
        {
            var person = _personRepository.GetPersonById(id);

            return person;

        }
    }
}
