﻿using MinhaAgenda.Services.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhaAgenda.Services.Interfaces.Services
{
    public interface IPersonService
    {

        Person CreatePerson(Person person);

        Person GetPersonById(int id);

        IEnumerable<Person> GetAllPersons();

    }
}
