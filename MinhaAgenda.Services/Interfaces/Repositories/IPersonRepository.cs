﻿using MinhaAgenda.Services.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhaAgenda.Services.Interfaces.Repositories
{
    public interface IPersonRepository
    {

        void InsertPerson(Person person);

        Person GetPersonById(int id);

        IEnumerable<Person> GetAllPersons();

    }
}
