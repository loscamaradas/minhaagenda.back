﻿using FluentValidation;
using MinhaAgenda.Data.Repositories;
using MinhaAgenda.Models.Person;
using MinhaAgenda.Models.Validation;
using MinhaAgenda.Services;
using MinhaAgenda.Services.Entities;
using MinhaAgenda.Services.Interfaces.Repositories;
using MinhaAgenda.Services.Interfaces.Services;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace MinhaAgenda.IoC
{
    public static class IoCRegister
    {

        public static Container Initialize(HttpConfiguration apiConfiguration)
        {

            var container = new Container();

            //container.Options.DefaultScopedLifestyle = new ExecutionContextScopeLifestyle();

            InitializeContainer(container);

            container.RegisterWebApiControllers(apiConfiguration);

            container.Verify();

            return container;

        }

        private static void InitializeContainer(Container container)
        {
            container.Register<IValidator<CreatePersonModel>, CreatePersonValidator>();
            container.Register<IPersonRepository, PersonRepository>();
            container.Register<IPersonService, PersonService>();
        }
    }
}
